﻿namespace GTAServer.PluginAPI.Entities
{
    public class Command
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Usage { get; set; }
    }
}
